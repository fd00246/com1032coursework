/**
 * ProcessScheduler.java
 */
package com.com1032.pss;

import java.util.ArrayList;
import java.util.List;

import com.com1032.assignment.processscheduler.Clock;
import com.com1032.assignment.processscheduler.ProcessState;
import com.com1032.assignment.processscheduler.SchedulingAlgorithm;

/**
 * @author felipedabrantes
 *
 */
public class ProcessSchedulerSimulator {
	/**New processes added to the system.*/
	private List<Process> jobQueue = new ArrayList<Process> ();
	/**Processes ready to be executed.*/
	private List<Process> readyQueue = new ArrayList<Process> ();
	/**Processes that have finished executing.*/
	private List<Process> terminatedQueue = new ArrayList<Process> ();
	
	/**The latest ran process.*/
	private Process scheduledProcess = null;
	
	/**The time spent running of the previous process.*/
	private int previousProcessTimeSpent = 0;
	
	/**The global time of the system.*/
	private Clock globalTime = null;
	
	/**The info report of the process scheduler.*/
	private StringBuffer report = new StringBuffer();
	
	
	/**
	 * Constructor. Sets global time.
	 * 
	 * @param globalTime
	 */
	public ProcessSchedulerSimulator(Clock globalTime) {
		this.globalTime = globalTime;
	}
	
	
	/**
	 * @return report of scheduler as string.
	 */
	public String getReport() {
		return this.report.toString();
	}
	
	
	/**
	 * Main function to run processes with the desired algorithm.
	 * 
	 * Quantum is needed for specific algorithms.
	 * 
	 * @param algorithm to use.
	 * 
	 * @param quantum for round robin algorithm.
	 */
	public void run(SchedulingAlgorithm algorithm, int quantum) {
		
		//Adding configuration info.
		this.report.insert(0, "---Process Scheduler---\n* Algorithm: " + algorithm + "\n* Quantum: " + quantum + "\n\n");
		
		while(!this.jobQueue.isEmpty() || !this.readyQueue.isEmpty() || this.scheduledProcess != null) {		
			
			//---Arrival---
			//Update ready queue to see if any processes are ready.
			this.updateReadyQueue();
			
			
			//---Termination Check---
			//Checks if any processes have finished.
			if(this.scheduledProcess != null) {
				//Terminates process if process is done.
				if(this.scheduledProcess.getRemainingTime() == 0) {
					this.terminateProcess(this.scheduledProcess);
				}	
			}
		
			//---Empty Ready Queue---
			//If no processes are ready yet, increase time by 1.
			if(this.readyQueue.isEmpty()) {
				
				if(this.scheduledProcess != null) {
					
					//In case of round robin, resets the time spent when above quantum.
					if(algorithm == SchedulingAlgorithm.RR && this.previousProcessTimeSpent >= quantum) {
						this.previousProcessTimeSpent = 0;
					}
					
					this.runProcess(this.scheduledProcess);
				}
				else {
					this.globalTime.increaseTime(1);
				}
				
				continue;
			}
			
			//---Scheduling---
			//Preemptive means processes can be interrupted. Constantly checked.
			//Non-Preemptive means processes run until finished.
			switch(algorithm) {
			case FIFO:
				runFIFO();
				break;
			case SRTF:
				runSRTF();
				break;
			case SPF:
				runSPF();
				break;
			case RR:
				runRR(quantum);
				break;
			}
			
			//---Running---
			//Run the scheduled process, if any.
			if(this.scheduledProcess != null) {
				this.runProcess(this.scheduledProcess);
			}
			
		}
		
	}
	
	
	/**
	 * Adds process to system (job queue).
	 * 
	 * @param process to add to system.
	 */
	public void addProcessToJob(Process process) {
		this.jobQueue.add(process);
		this.report.append("\n- Added to Job Queue: " + process + "\n");
	}
	
	
	/**
	 * Adds processes given in list to system (job queue).
	 * 
	 * @param processes list to add to system.
	 */
	public void addProcessToJob(List<Process> processes) {
		for(Process process: processes) {
			this.jobQueue.add(process);
			this.report.append("\n- Added to Job Queue @ " + this.globalTime.getTime() + ":" + process + "\n");
		}
	}
	
	
	/**
	 * Function used to schedule a process to CPU.
	 * 
	 * @param process to schedule.
	 */
	public void scheduleProcess(Process process) {
		//Only schedules if not running already.
		if(this.scheduledProcess != process) {
			
			this.previousProcessTimeSpent = 0;
			
			//Ran if there is already a process running.
			if(this.scheduledProcess != null) {
				this.contextSwitch(process);
			}
			
			//Update scheduler attributes.
			this.report.append("\n- Schedule @ " + this.globalTime.getTime() + ":" + process + "\n");
			process.setState(ProcessState.RUNNING);
			this.scheduledProcess = process;
			this.readyQueue.remove(process);
		}
	}
	
	
	/**
	 * Function used to run a process.
	 * 
	 * @param process to run.
	 */
	public void runProcess(Process process) {
		this.previousProcessTimeSpent++;
		process.setRemainingTime(process.getRemainingTime() - 1);
		this.globalTime.increaseTime(1);
	}
	
	
	/**
	 * Function used when terminating a process.
	 * 
	 * @param process to terminate.
	 */
	public void terminateProcess(Process process) {
		//Update process properties.
		process.setCompletionTime(this.globalTime.getTime());
		process.setState(ProcessState.TERMINATED);
		
		//Update scheduler info.
		this.scheduledProcess = null;
		this.readyQueue.remove(process);
		this.terminatedQueue.add(process);
		this.report.append("\n- Terminated @ " + this.globalTime.getTime() + ":" + process + "\n");
	}
	
	
	/**
	 * Function used when suspending a process.
	 * 
	 * @param process to suspend.
	 */
	public void contextSwitch(Process process) {
		this.readyQueue.add(this.scheduledProcess);
		this.report.append("\n- Suspending @ " + this.globalTime.getTime() + ":" + this.scheduledProcess + "\n");
		this.scheduledProcess.setState(ProcessState.READY);
	}
	
	
	/**
	 * Updates ready queue by checking arrival times of processes in job queue.
	 * 
	 * If arrival time equals global time add it to ready queue.
	 */
	public void updateReadyQueue() {
		List<Process> updatedProcesses = new ArrayList<Process> ();
		
		for(Process process: this.jobQueue) {
			if(process.getArrivalTime() == this.globalTime.getTime()) {
				updatedProcesses.add(process);
			}
		}
		
		for(Process process: updatedProcesses) {
			process.setState(ProcessState.READY);
			this.jobQueue.remove(process);
			this.readyQueue.add(process);
			
			this.report.append("\n- Added to Ready Queue @ " + this.globalTime.getTime() + ":" + process + "\n");
		}
	}	
	
	/**
	 * Runs the FIFO algorithm.
	 * 
	 * First-In-First-Out.
	 */
	public void runFIFO() {
		//Non-Preemptive, won't access new process from ready queue if one is already running.
		if(this.scheduledProcess == null) {
			Process currentProcess = this.readyQueue.get(0);
			this.scheduleProcess(currentProcess);
		}
		else {
			this.scheduleProcess(this.scheduledProcess);
		}
		
	}
	
	
	/**
	 * Runs the SPF algorithm.
	 * 
	 * Shortest Process First.
	 */
	public void runSPF() {
		//Non-Preemptive, won't access new process from ready queue if one is already running.
		if(this.scheduledProcess == null) {
			
			//Find smallest burst time process.
			Process currentProcess = this.readyQueue.get(0);
			for(Process process: this.readyQueue) {
				if(process.getBurstTime() < currentProcess.getBurstTime()) {
					currentProcess = process;
				}
			}
			
			this.scheduleProcess(currentProcess);
		}
		else {
			this.scheduleProcess(this.scheduledProcess);
		}
		
	}
	
	
	/**
	 * Runs the SRTF algorithm.
	 * 
	 * Shortest Remaining Time First.
	 */
	public void runSRTF() {
		//Find smallest remaining time process.
		Process currentProcess = this.readyQueue.get(0);
		for(Process process: this.readyQueue) {
			if(process.getRemainingTime() < currentProcess.getRemainingTime()) {
				currentProcess = process;
			}
		}
		
		this.scheduleProcess(currentProcess);
		
	}
	
	
	/**
	 * Runs the RR algorithm with the given quantum..
	 * 
	 * Round Robin.
	 * 
	 * @param quantum to use.
	 */
	public void runRR(int quantum) {
		Process currentProcess = this.readyQueue.get(0);
		
		if(this.previousProcessTimeSpent >= quantum || this.scheduledProcess == null) {
			this.scheduleProcess(currentProcess);
		}
	}
	
}
