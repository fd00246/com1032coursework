/**
 * Process.java
 */
package com.com1032.pss;

import com.com1032.assignment.processscheduler.ProcessState;

/**
 * @author felipedabrantes
 *
 */
public class Process {
	/**The unique ID to identify the process.*/
	private int id = 0;
	/**The name of the process.*/
	private String name = null;
	/**The state of the process.*/
	private ProcessState state = ProcessState.NEW;
	
	//---Attributes for process scheduling. ---
	/**States the importance of the process. Higher is more important.*/
	private int priority = 0;
	/**Global time at which process arrives to ready queue.*/
	private int arrivalTime = 0;
	/**Global time at which process completes its execution.*/
	private int completionTime = -1;
	/**Time required for CPU execution.*/
	private final int burstTime;
	/**Time remaining for process finished.*/
	private int remainingTime = 0;
	
	
//	private List<Thread> threads = new ArrayList<Thread> ();
	
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param id
	 * @param state
	 * @param priority
	 * @param arrivalTime
	 * @param completionTime
	 * @param burstTime
	 */
	public Process(int id, String name, int priority, int burstTime, int arrivalTime) {
		super();
		this.id = id;
		this.name = name;
		this.priority = priority;
		this.arrivalTime = arrivalTime;
		this.burstTime = burstTime;
		this.remainingTime = burstTime;
	}


	/**
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public void setState(ProcessState state) {
		this.state = state;
	}


	/**
	 * @param completionTime
	 */
	public void setCompletionTime(int completionTime) {
		this.completionTime = completionTime;
	}
	
	
	/**
	 * @param remainingTime
	 */
	public void setRemainingTime(int remainingTime) {
		this.remainingTime = remainingTime;
	}

	/**
	 * @param arrivalTime
	 */
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return the state
	 */
	public ProcessState getState() {
		return this.state;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return this.priority;
	}

	/**
	 * @return the arrivalTime
	 */
	public int getArrivalTime() {
		return this.arrivalTime;
	}

	/**
	 * @return the completionTime
	 */
	public int getCompletionTime() {
		return this.completionTime;
	}
	
	/**
	 * @return the burstTime
	 */
	public int getBurstTime() {
		return this.burstTime;
	}
	
	/**
	 * @return the burstTime
	 */
	public int getRemainingTime() {
		return this.remainingTime;
	}



	public String toString() {
		
		StringBuffer info = new StringBuffer();
		
		info.append("\nProcess #");
		info.append(this.id);
		info.append(" | Name: ");
		info.append(this.name);
		info.append(" | State: ");
		info.append(this.state);
		info.append(" | Arrival Time: ");
		info.append(this.arrivalTime);
		info.append(" | Burst Time: ");
		info.append(this.burstTime);
		info.append(" | Remaining Time: ");
		info.append(this.remainingTime);
		info.append(" | Completion Time: ");
		info.append(this.completionTime);
		
		return info.toString(); 
		
	}

}
