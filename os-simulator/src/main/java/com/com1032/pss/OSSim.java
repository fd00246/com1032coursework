package com.com1032.pss;
/**
 * OS.java
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.com1032.assignment.processscheduler.Clock;
import com.com1032.assignment.processscheduler.SchedulingAlgorithm;
import com.com1032.pss.Process;
import com.com1032.pss.ProcessSchedulerSimulator;

/**
 * @author felipedabrantes
 *
 */
public class OSSim {

	/**The global time of the OS.*/
	private Clock globalTime = new Clock();
	/**The class the handles the process scheduling.*/
	private ProcessSchedulerSimulator pss = null;
	/**A list of processes of the OS.*/
	private List<Process> processes = new ArrayList<Process>();

	
	/**
	 * Constructor. Initialises fields.
	 */
	public OSSim() {
		this.pss = new ProcessSchedulerSimulator(this.globalTime);
	}

	/**
	 * @param args
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void main(String[] args) throws FileNotFoundException {
		OSSim os = new OSSim();

		os.addProcess("OSInput.txt");
		os.runProcessScheduling();

	}

	
	/**
	 * Used to a single process to OS.
	 * @param process
	 */
	public void addProcess(Process process) {
		this.processes.add(process);
	}
	
	
	/**
	 * Used to add processes from a file to the OS.
	 * 
	 * @param fileName of the processes file.
	 * 
	 * @throws FileNotFoundException if no file is found.
	 */
	public void addProcess(String fileName){
		//Tries to open file.
		try {
			File file = new File(fileName);
			Scanner fileReader = new Scanner(file);
			
			//Reads file line by line.
			while (fileReader.hasNextLine()) {
				//Splits process info to make a process object.
				String[] data = fileReader.nextLine().split(", ");
				this.processes.add(new Process(
						Integer.valueOf(data[0]), 
						data[1], 
						Integer.valueOf(data[4]), 
						Integer.valueOf(data[3]), 
						Integer.valueOf(data[2])));
			}
			
			//Closes file.
			fileReader.close();
		}
		//Ran if no file is found.
		catch(FileNotFoundException e) {
			System.out.println("File not found.");
		}

	}

	
	/**
	 * Runs process scheduling simulation with processes in OS.
	 */
	public String runProcessScheduling() {
		pss.addProcessToJob(this.processes);
		pss.run(SchedulingAlgorithm.SPF, 3);
		return pss.getReport();
	}

}