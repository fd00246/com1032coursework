/**
 * IOSystem.java
 */
package com.com1032.assignment.IOsystem;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Interface class containing necessary functions for interacting with IO devices in a system.
 * 
 * @author felipedabrantes
 */
public interface IOSystem {
	ArrayList<MemoryMappedFile> peripherals = new ArrayList<MemoryMappedFile>();
	
	public void addDevice (String deviceName, String BufferFileName, IOType deviceType) throws IllegalArgumentException, IOException;
	public void removeDevice(String deviceName) throws IllegalArgumentException, IOException;
	
	public void write(String deviceName, String inputData) throws IllegalArgumentException, IOException;
	public String read(String deviceName, int size) throws IllegalArgumentException, IOException;
	public void clear(String deviceName) throws IllegalArgumentException, IOException;
	
}
