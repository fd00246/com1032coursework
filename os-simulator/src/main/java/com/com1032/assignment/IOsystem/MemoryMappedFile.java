/**
 * MemoryMappedFile.java
 */
package com.com1032.assignment.IOsystem;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;


/**
 * Class that manages peripherals and interacts with memory mapped files.
 * 
 * @author felipedabrantes
 */
public class MemoryMappedFile {

	/**The peripheral name.*/
	private String deviceName = null;
	/**The peripheral buffer file name.*/
	private String filename = null;
	/**The length of data in the file*/
	private int dataLength = 0;
	
	/**Whether peripheral is input or output.*/
	private IOType type;
	
	/**The directory where the buffer folders are stored.*/
	private final String BUFFERS_DIRECTORY;
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param bufferDirectory where the buffer folders are stored.
	 * @param deviceName to add.
	 * @param filename of buffer file.
	 * @param type of IO device.
	 * 
	 * @throws IOException if there is an error interacting with memory file.
	 * 
	 */
	public MemoryMappedFile (String bufferDirectory, String deviceName, String filename, IOType type) throws IOException {
		this.BUFFERS_DIRECTORY = bufferDirectory;
		
		if (deviceName != null)
			this.setDeviceName(deviceName);
		else
			this.setDeviceName("");
		
		this.filename = filename;
		this.type = type;
		
		this.create();
	}
	
	
	/**
	 * Function used to write to file in memory without having to open all of file.
	 * 
	 * @param bytesBuffer to add to file.
	 * 
	 * @throws IllegalArgumentException if device does not exist.
	 * @throws IOException if there is an error interacting with memory file.
	 */
	public void write(Byte[] bytesBuffer) throws IllegalArgumentException, IOException{	
		//Checks peripheral is output type.
		if(this.type == IOType.INPUT) {
			throw new IllegalArgumentException("Error: Input peripheral cannot output data.");
		}
		
		else {
			//Size of data being added to file.
			int size = bytesBuffer.length;
			
			RandomAccessFile memoryMappedFile;
			try {
				//Opens file.
				memoryMappedFile = new RandomAccessFile(this.BUFFERS_DIRECTORY + this.filename + ".txt", "rw");
	
				//Lets us write to file in memory without having to access all of it.
				MappedByteBuffer out = memoryMappedFile.getChannel().map(FileChannel.MapMode.READ_WRITE, this.dataLength, size);
				
				//Writing bit data Memory Mapped File
				for (int i = 0; i < size; i++) {
					out.put(bytesBuffer[i]);
				}
				
				//Increases data length attribute by size being added to file.
				this.dataLength += size;
				
				memoryMappedFile.close();
				
			} catch (IOException e) {
				throw new IOException("Error using " + this.filename + ".");
			}	
		}
	}
	
	
	/**
	 * Function used to read from a file in memory without having to open all of file.
	 * 
	 * @param size amount from file to read.
	 * 
	 * @return byte data.
	 * 
	 * @throws IllegalArgumentException if device does not exist.
	 * @throws IOException if there is an error interacting with memory file.
	 */
	public Byte[] read(int size) throws IllegalArgumentException, IOException{
		//Checks peripheral is output type.
		if(this.type == IOType.OUTPUT) {
			throw new IllegalArgumentException("Error: Input peripheral cannot output data.");
		}
		
		//The desired byte data.
		Byte[] wantedData = new Byte[size];
		
		RandomAccessFile memoryMappedFile;
		
		try {
			//Open file.
			memoryMappedFile = new RandomAccessFile(this.BUFFERS_DIRECTORY + this.filename + ".txt", "rw");

			//Lets us read file in memory without having to access all of it.
			MappedByteBuffer in = memoryMappedFile.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, size);
			
			//Reading from memory file.
			for (int i = 0; i < size ; i++) {
				wantedData[i] = in.get(i);
			}
			
			memoryMappedFile.close();
			
		} catch (IOException e) {
			throw new IOException("Error using " + this.filename + ".");
		}
		
		return wantedData;
	}

	
	/**
	 * Function used to clear the file in memory without having to open all of file.
	 * 
	 * @throws IOException if there is an error interacting with memory file.
	 */
	public void clear() throws IOException {
		//Opens file and deletes it.
		File file = new File(this.BUFFERS_DIRECTORY + this.filename + ".txt");
		file.delete();
		
		this.dataLength = 0;
		
		this.create();
	}
	
	
	/**
	 * Function used to create the buffer file.
	 * 
	 * @throws IOException if there is an error interacting with memory file.
	 */
	public void create() throws IOException {
		RandomAccessFile memoryMappedFile;
		try {
			//Creates file.
			memoryMappedFile = new RandomAccessFile(this.BUFFERS_DIRECTORY + this.filename + ".txt", "rw");
			memoryMappedFile.close();
			
		} catch (IOException e) {
			throw new IOException("Error using file.");
		}
	}
	
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return this.deviceName;
	}

	/**
	 * @param deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return this.filename;
	}
}
