/**
 * JavaIOSystem.java
 */
package com.com1032.assignment.IOsystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.com1032.assignment.processscheduler.Hardware;

/**
 * A class to control all of the IO devices in the system.
 * 
 * @author felipedabrantes
 */
public class JavaIOSystem implements IOSystem{
	/**A list of peripherals stored as Memory Mapped File objects.*/
	private ArrayList<MemoryMappedFile> peripherals = new ArrayList<MemoryMappedFile>();
	
	/**Directory name of folder locations to create.*/
	private String directoryName = null;
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param directoryName 
	 * @param hardware
	 * 
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws IndexOutOfBoundsException if there is an error with hardware file parameters.
	 */
    public JavaIOSystem(String directoryName, Hardware hardware) throws IOException, IndexOutOfBoundsException {
    	
    	this.directoryName = directoryName;
    	
		try {
			//Makes new directories.
			new File(directoryName + "/Peripherals/Buffers").mkdirs();

			//Clears directories of any files.
			for(File file: (new File(directoryName + "/Peripherals/Buffers")).listFiles()) {
				if(!file.isDirectory()) {
					file.delete();
				}
			}
			
			//Accesses peripherals info from hardware given.
			for(String peripheralInfo: hardware.getPeripheralsInfo()) {
				//DeviceName, BufferFileName, IODeviceType
				String deviceName = peripheralInfo.split(" ")[0];
				String bufferFileName = peripheralInfo.split(" ")[1];
				IOType deviceType;
				
            	if(peripheralInfo.split(" ")[2].toLowerCase().equals("i")) {
            		deviceType = IOType.INPUT;
            	}
            	else {
            		deviceType = IOType.OUTPUT;
            	}
            	
            	//Adds device to system.
            	this.addDevice(deviceName, bufferFileName, deviceType);
			}
			
		}
		
		//Errors that can happen with files.
		catch(IOException e) {
			throw new IOException("Error adding buffer folders.");
		}
		catch(IndexOutOfBoundsException e) {
			throw new IndexOutOfBoundsException("Error with Hardware file parameters.");
		}
    }
    
    
	/**
	 * Adds a device to the IO System.
	 * 
	 * Checks names are not already in system.
	 * 
	 * @param deviceName to add.
	 * @param BufferFileName
	 * @param deviceType, input or output.
	 * 
	 * @throws IllegalArgumentException if device info is invalid.
	 * @throws IOException if there is an error interacting with memory file.
	 */
    @Override
    public void addDevice(String deviceName, String bufferFileName, IOType deviceType) throws IllegalArgumentException, IOException {
    	//True when file name is already in system.
    	boolean error = false;
    	
    	//Loops to check device does not already exist.
    	for(MemoryMappedFile peripheral: this.peripherals) {
    		if(peripheral.getDeviceName().equals(deviceName)) {
    			error = true;
    			throw new IllegalArgumentException("Error: Device with same name already in system.");
    		}
    		
    		if(peripheral.getFileName().equals(bufferFileName)) {
    			error = true;
    			throw new IllegalArgumentException("Error: Buffer File with same name already in system.");
    		}
    	}
    	
    	//If device name and file name is valid.
    	if(error == false) {
    		MemoryMappedFile newPeripheral = new MemoryMappedFile(this.directoryName + "/Peripherals/Buffers/", deviceName, bufferFileName, deviceType);
    		this.peripherals.add(newPeripheral);	
    	}
    }

    
	/**
	 * Removes device from IO System.
	 * 
	 * @param deviceName to remove.
	 * 
	 * @throws IllegalArgumentException if device info is invalid.
	 * @throws IOException if there is an error interacting with memory file.
	 */
	@Override
	public void removeDevice(String deviceName) throws IOException, IllegalArgumentException {
		boolean deviceFound = false;
		
		//Loops to find device with given parameter.
		for (MemoryMappedFile peripheral: this.peripherals) {
			if (peripheral.getDeviceName().equals(deviceName)) {
				deviceFound = true;
				peripheral.clear();
				peripherals.remove(peripheral);
				break;
			}
		}
		
		if(deviceFound == false) {
			throw new IllegalArgumentException("Error: Device not found.");
		}
	}
	
	
    /**
     * Writes at the end of the given device buffer file in memory.
     * 
     * @param deviceName
     * @param inputData to write.
     * 
	 * @throws IllegalArgumentException if device info is invalid.
	 * @throws IOException if there is an error interacting with memory file.
     */
    @Override
	public void write(String deviceName, String inputData) throws IllegalArgumentException, IOException{
        //Turn string input data into bytes array.
        byte[] dataArray = inputData.getBytes();
        
        //Turn byte[] into Byte[] for Memory Mapped Files.
        Byte[] bytesBuffer = new Byte[dataArray.length];
        
        for(int i = 0; i < bytesBuffer.length; i++) {
        	bytesBuffer[i] = dataArray[i];
        }
       
        
    	//Find the Memory Mapped File for the device and write to it.
        boolean deviceFound = false;
		for (MemoryMappedFile peripheral: this.peripherals) {
			if (peripheral.getDeviceName().equals(deviceName)) {
				deviceFound = true;
				peripheral.write(bytesBuffer);
			}
		}
		
		if(deviceFound == false) {
			throw new IllegalArgumentException("Error: Device " + deviceName + " not found.");
		}
	}

    
    /**
     * Function to read the given device buffer file in memory.
     * 
     * @param deviceName
     * @param size to read.
     * 
	 * @throws IllegalArgumentException if device info is invalid.
	 * @throws IOException if there is an error interacting with memory file.
     */
    @Override
	public String read(String deviceName, int size) throws IllegalArgumentException, IOException{
		///The wanted data as a string.
		String requiredData = null;
		
		//Bytes accessed from file.
		Byte[] bytesBuffer = null;
		
		//Find the Memory Mapped File for the device and read from it.
		boolean deviceFound = false;
		for (MemoryMappedFile peripheral: this.peripherals) {
			if (peripheral.getDeviceName().equals(deviceName)) {
				deviceFound = true;
				bytesBuffer = peripheral.read(size);
			}
		}
		
		if(deviceFound == false) {
			throw new IllegalArgumentException("Error: Device " + deviceName + " not found.");
		}
		
		int bytesArraySize = 0;
		
		for(int j = 0; j < bytesBuffer.length; j++) {
			if(bytesBuffer[j] != 0) {
				bytesArraySize++;
			}
		}
		
		if(bytesBuffer != null) {
			//Turn Byte[] into byte[].
	        byte[] bytes = new byte[bytesArraySize];
	        
	        for(int j = 0; j < bytesBuffer.length; j++) {
	        	if(bytesBuffer[j] != 0) {
	        		bytes[j] = bytesBuffer[j].byteValue();
	        	}
	        }
	        
	        requiredData = new String(bytes);
		}
        
		return requiredData;
	}


    /**
     * Function to clear the given device buffer file.
     * 
     * @param deviceName file to clear.
     * 
	 * @throws IllegalArgumentException if device info is invalid.
	 * @throws IOException if there is an error interacting with memory file.
     */
	@Override
	public void clear(String deviceName) throws IllegalArgumentException, IOException {
		boolean deviceFound = false;
		
    	//Find the Memory Mapped File for the device and clear it.
		for (MemoryMappedFile peripheral: this.peripherals) {
			if (peripheral.getDeviceName().equals(deviceName)) {
				deviceFound = true;
				peripheral.clear();
				break;
			}
		}
		
		if(deviceFound == false) {
			throw new IllegalArgumentException("Error: Device " + deviceName + " not found.");
		}
	}

}
