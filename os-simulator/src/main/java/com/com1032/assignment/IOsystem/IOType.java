/**
 * IOType.java
 */
package com.com1032.assignment.IOsystem;

/**
 * Enumeration class to state the type an IO device can be.
 * 
 * @author felipedabrantes
 */
public enum IOType {
	/**Device is an Input.*/
	INPUT,
	
	/**Device is an Output.*/
	OUTPUT
	
}
