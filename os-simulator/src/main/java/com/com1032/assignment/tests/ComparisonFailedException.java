/**
 * 
 */
package com.com1032.assignment.tests;

/**
 * @author felipedabrantes
 *
 */
public class ComparisonFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public ComparisonFailedException(String message) {
		super(message);
	}

}
