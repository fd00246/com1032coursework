/**
 * IOSystemTest.java
 */
package com.com1032.assignment.tests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import com.com1032.assignment.processscheduler.OS;
import com.com1032.assignment.processscheduler.SchedulingAlgorithm;

/**
 * Test class with static functions to test IO devices of the OS.
 * 
 * @author felipedabrantes
 */
public class IOSystemTest {
	
	
	/**
	 * Tests the output device buffer files.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void OutputDeviceTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		os.getPs().setAlgorithm(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		//File contains a print statement to device printer.
		AllTests.copyProcessFile(os, "blocked4_1.txt");
		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(2000);
		
		File file = new File(os.getDirectoryName() + "/Peripherals/Buffers/printerBuffer.txt");
		Scanner fileReader = new Scanner(file);
		String actualResult = fileReader.nextLine();
		String expectedResult = "1";
		fileReader.close();
		
		if(!actualResult.equals(expectedResult)) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Tests the input device buffer files.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void InputDeviceTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Write to keyboard.
		String textToAppend = "20";
		BufferedWriter writer = new BufferedWriter(new FileWriter(os.getDirectoryName() + "/Peripherals/Buffers/keyboardBuffer.txt", true));
		writer.write(textToAppend);
		writer.close();
		
		//Copy files from test.
		//File contains a print statement to device printer.
		AllTests.copyProcessFile(os, "blocked2_1.txt");
		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(2000);
		
		File file = new File(os.getDirectoryName() + "/Peripherals/Buffers/printerBuffer.txt");
		Scanner fileReader = new Scanner(file);
		String actualResult = fileReader.nextLine();
		String expectedResult = "20";
		fileReader.close();
		
		if(!actualResult.equals(expectedResult)) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
}
