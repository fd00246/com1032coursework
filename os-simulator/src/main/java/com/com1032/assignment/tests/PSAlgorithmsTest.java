/**
 * PSAlgorithmsTest.java 
 */
package com.com1032.assignment.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.com1032.assignment.processscheduler.OS;
import com.com1032.assignment.processscheduler.SchedulingAlgorithm;

/**
 * Test class with static functions to test the scheduling algorithms of the OS.
 * 
 * @author felipedabrantes
 */
public class PSAlgorithmsTest {
	
	/**
	 * Tests the FIFO algorithm.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void FIFOProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {		
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		AllTests.copyProcessFile(os, "terminate14.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate70.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate4.txt");
		
		TimeUnit.MILLISECONDS.sleep(6000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready terminate14 NEW false\n");
		expectedReport.append("Schedule terminate14 NEW false\n");
		expectedReport.append("Ready terminate70 NEW false\n");
		expectedReport.append("Ready terminate4 NEW false\n");
		expectedReport.append("Terminated terminate14 TERMINATED false\n");
		expectedReport.append("Schedule terminate70 NEW false\n");
		expectedReport.append("Terminated terminate70 TERMINATED false\n");
		expectedReport.append("Schedule terminate4 NEW false\n");
		expectedReport.append("Terminated terminate4 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Tests the SPF algorithm.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void SPFProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//Start OS with SPF algorithm.
		os.turnOn(SchedulingAlgorithm.SPF, 5);
		
		//Copy files from test first.
		AllTests.copyProcessFile(os, "terminate70.txt");
		TimeUnit.MILLISECONDS.sleep(500);
		AllTests.copyProcessFile(os, "terminate14.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate4.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate28.txt");

		TimeUnit.MILLISECONDS.sleep(7000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready terminate70 NEW false\n");
		expectedReport.append("Schedule terminate70 NEW false\n");
		expectedReport.append("Ready terminate14 NEW false\n");
		expectedReport.append("Ready terminate4 NEW false\n");
		expectedReport.append("Ready terminate28 NEW false\n");
		expectedReport.append("Terminated terminate70 TERMINATED false\n");
		expectedReport.append("Schedule terminate4 NEW false\n");
		expectedReport.append("Terminated terminate4 TERMINATED false\n");
		expectedReport.append("Schedule terminate14 NEW false\n");
		expectedReport.append("Terminated terminate14 TERMINATED false\n");
		expectedReport.append("Schedule terminate28 NEW false\n");
		expectedReport.append("Terminated terminate28 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Tests the SRTF algorithm.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void SRTFProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//Start OS with SPF algorithm.
		os.turnOn(SchedulingAlgorithm.SRTF, 5);
		
		//Add files.
		AllTests.copyProcessFile(os, "terminate14.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate4.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate28.txt");

		TimeUnit.MILLISECONDS.sleep(3000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		os.turnOff();
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready terminate14 NEW false\n");
		expectedReport.append("Schedule terminate14 NEW false\n");
		expectedReport.append("Ready terminate4 NEW false\n");
		expectedReport.append("Suspending terminate14 RUNNING false\n");
		expectedReport.append("Schedule terminate4 NEW false\n");
		expectedReport.append("Ready terminate28 NEW false\n");
		expectedReport.append("Terminated terminate4 TERMINATED false\n");
		expectedReport.append("Schedule terminate14 READY false\n");
		expectedReport.append("Terminated terminate14 TERMINATED false\n");
		expectedReport.append("Schedule terminate28 NEW false\n");
		expectedReport.append("Terminated terminate28 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
	}
	
	
	/**
	 * Tests the RR algorithm. Quantum 5.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void RRProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//Start OS with SPF algorithm.
		os.turnOn(SchedulingAlgorithm.RR, 5);
		
		//Add files.
		AllTests.copyProcessFile(os, "terminate14.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate4.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate28.txt");

		TimeUnit.MILLISECONDS.sleep(3000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready terminate14 NEW false\n");
		expectedReport.append("Schedule terminate14 NEW false\n");
		expectedReport.append("Ready terminate4 NEW false\n");
		expectedReport.append("Ready terminate28 NEW false\n");
		expectedReport.append("Suspending terminate14 RUNNING false\n");
		expectedReport.append("Schedule terminate4 NEW false\n");
		expectedReport.append("Terminated terminate4 TERMINATED false\n");
		expectedReport.append("Schedule terminate28 NEW false\n");
		expectedReport.append("Suspending terminate28 RUNNING false\n");
		expectedReport.append("Schedule terminate14 READY false\n");
		expectedReport.append("Suspending terminate14 RUNNING false\n");
		expectedReport.append("Schedule terminate28 READY false\n");
		expectedReport.append("Suspending terminate28 RUNNING false\n");
		expectedReport.append("Schedule terminate14 READY false\n");
		expectedReport.append("Terminated terminate14 TERMINATED false\n");
		expectedReport.append("Schedule terminate28 READY false\n");
		expectedReport.append("Terminated terminate28 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}

}
