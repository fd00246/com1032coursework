/**
 * AllTests.java
 */
package com.com1032.assignment.tests;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import com.com1032.assignment.processscheduler.OS;

/**
 * Class with static functions to run all available tests for OS.
 * 
 * @author felipedabrantes
 */
public class AllTests {
	
	/**
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void runAllTests(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//IO devices tests.
		IOSystemTest.OutputDeviceTest(os);
		IOSystemTest.InputDeviceTest(os);
		
		//Algorithms tests.
		PSAlgorithmsTest.FIFOProcessTest(os);
		PSAlgorithmsTest.SPFProcessTest(os);
		PSAlgorithmsTest.SRTFProcessTest(os);
		PSAlgorithmsTest.RRProcessTest(os);
		
		//Blocked Processes tests.
		PSBlockedTest.FIFOBlockedProcessTest(os);
		PSBlockedTest.RRBlockedProcessTest(os);
		PSBlockedTest.ErrorBlockedProcessTest(os);
		PSBlockedTest.ErrorReportBlockedProcessTest(os);
		
	}
	
	
	/**
	 * Copies the required test process from '_Tests' folder to 'New_Processes'.
	 * 
	 * @param os
	 * @param fileName of process to move.
	 * 
	 * @throws IOException
	 */
	static public void copyProcessFile(OS os, String fileName) throws IOException {
		try {
			Path testSource = Paths.get(os.getDirectoryName() + "/Processes/_Tests/" + fileName);
			Path target = Paths.get(os.getDirectoryName() + "/Processes/New_Processes/" + fileName);
			Files.copy(testSource, target, StandardCopyOption.REPLACE_EXISTING);
		}
		catch(IOException e) {
			throw new IOException("Error accessing '_Tests' folder. Make sure it is in 'Processes' folder.");
		}
	}
	
	
	/**
	 * Generates a simpler process report from the report given.
	 * 
	 * @param report
	 * 
	 * @return simplified report.
	 */
	static public String simplifyProcessReport(String report) {
		StringBuffer simpleReport = new StringBuffer();
		
		String action = null;
		String name = null;
		String state = null;
		boolean failed = false;
		
		String[] split = report.split("\n");
		
		for(String line: split) {
			if(!line.equals("")) {
				
				action = line.split(" ")[1];
				name = line.split(" ")[8];
				
				if(line.split(" ")[10].equals("FAILED")) {
					failed = true;
					state = line.split(" ")[13];
				}
				else {
					state = line.split(" ")[11];
				}
				
				simpleReport.append(action + " ");
				simpleReport.append(name + " ");
				simpleReport.append(state + " ");
				simpleReport.append(failed);
				simpleReport.append("\n");
				
				failed = false;
			}
		}
		
		return simpleReport.toString();
		
	}
	
	
	/**
	 * Generates a simpler error report from the report given.
	 * 
	 * @param report
	 * 
	 * @return simplified error report.
	 */
	static public String simplifyErrorReport(String report) {
		StringBuffer simpleReport = new StringBuffer();
		
		String lineNum = null;
		String name = null;
		String state = null;
		
		String[] split = report.split("\n");
		
		for(String line: split) {
			
			if(!line.equals("")) {
				lineNum = line.split(" ")[5];
				name = line.split(" ")[10];
				state = line.split(" ")[13];
				
				simpleReport.append(lineNum + " ");
				simpleReport.append(name + " ");
				simpleReport.append(state);
				
				break;
			}
		}
		
		return simpleReport.toString();
		
	}
	

}
