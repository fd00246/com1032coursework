/**
 * PSBlockedTest.java
 */
package com.com1032.assignment.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.com1032.assignment.processscheduler.OS;
import com.com1032.assignment.processscheduler.SchedulingAlgorithm;

/**
 * Test class with static functions to test blocked processes in the OS.
 * 
 * @author felipedabrantes
 */
public class PSBlockedTest {
	
	/**
	 * Test used to test blocked processes with FIFO algorithm.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void FIFOBlockedProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		AllTests.copyProcessFile(os, "blocked4_1.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "blocked6_2.txt");
		TimeUnit.MILLISECONDS.sleep(200);
		AllTests.copyProcessFile(os, "blocked18_3.txt");
		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(5000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready blocked4_1 NEW false\n");
		expectedReport.append("Schedule blocked4_1 NEW false\n");
		expectedReport.append("Ready blocked6_2 NEW false\n");
		expectedReport.append("Blocking blocked4_1 BLOCKED false\n");
		expectedReport.append("Schedule blocked6_2 NEW false\n");
		expectedReport.append("Terminated blocked4_1 TERMINATED false\n");
		expectedReport.append("Ready blocked18_3 NEW false\n");
		expectedReport.append("Blocking blocked6_2 BLOCKED false\n");
		expectedReport.append("Schedule blocked18_3 NEW false\n");
		expectedReport.append("Terminated blocked6_2 TERMINATED true\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Ready blocked18_3 READY false\n");
		expectedReport.append("Schedule blocked18_3 READY false\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Ready blocked18_3 READY false\n");
		expectedReport.append("Schedule blocked18_3 READY false\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Terminated blocked18_3 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Test used to test blocked processes with FIFO algorithm.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void RRBlockedProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		AllTests.copyProcessFile(os, "blocked18_3.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "blocked4_1.txt");
		TimeUnit.MILLISECONDS.sleep(100);
		AllTests.copyProcessFile(os, "terminate14.txt");

		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(5000);
		
		String actualReport = os.getPs().getProcessReport();	
		String simpleReport = AllTests.simplifyProcessReport(actualReport);
		
		StringBuffer expectedReport = new StringBuffer();		
			
		expectedReport.append("Ready blocked18_3 NEW false\n");
		expectedReport.append("Schedule blocked18_3 NEW false\n");
		expectedReport.append("Ready blocked4_1 NEW false\n");
		expectedReport.append("Ready terminate14 NEW false\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Schedule blocked4_1 NEW false\n");
		expectedReport.append("Ready blocked18_3 READY false\n");
		expectedReport.append("Blocking blocked4_1 BLOCKED false\n");
		expectedReport.append("Schedule terminate14 NEW false\n");
		expectedReport.append("Terminated blocked4_1 TERMINATED false\n");
		expectedReport.append("Terminated terminate14 TERMINATED false\n");
		expectedReport.append("Schedule blocked18_3 READY false\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Ready blocked18_3 READY false\n");
		expectedReport.append("Schedule blocked18_3 READY false\n");
		expectedReport.append("Blocking blocked18_3 BLOCKED false\n");
		expectedReport.append("Terminated blocked18_3 TERMINATED false\n");
		
		if(!simpleReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Test used to test the invalid blocked processes, ones with errors.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void ErrorBlockedProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		//Code has a device not in hardware file.
		AllTests.copyProcessFile(os, "blockedInvalid6_1.txt");
		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(1000);
		
		String actualProcessReport = os.getPs().getProcessReport();
		String simpleProcessReport = AllTests.simplifyProcessReport(actualProcessReport);
		

		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("Ready blockedInvalid6_1 NEW false\n");
		expectedReport.append("Schedule blockedInvalid6_1 NEW false\n");
		expectedReport.append("Blocking blockedInvalid6_1 BLOCKED false\n");
		expectedReport.append("Terminated blockedInvalid6_1 TERMINATED true\n");
		
		if(!simpleProcessReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
	
	/**
	 * Test used to test the error report with blocked processes.
	 * 
	 * @param os
	 * 
	 * @throws IOException if there is an error accessing system files.
	 * @throws InterruptedException if there is an error with hardware file parameters.
	 * @throws ComparisonFailedException if comparison fails.
	 */
	static public void ErrorReportBlockedProcessTest(OS os) throws IOException, InterruptedException, ComparisonFailedException {
		//Start OS with FIFO algorithm..
		os.turnOn(SchedulingAlgorithm.FIFO, 5);
		
		//Copy files from test.
		//Code has a device not in hardware file.
		AllTests.copyProcessFile(os, "blockedInvalid6_1.txt");
		
		//Wait for processes to execute.
		TimeUnit.MILLISECONDS.sleep(1000);
		
		String actualErrorReport = os.getPs().getErrorReport();
		String simpleErrorReport = AllTests.simplifyErrorReport(actualErrorReport);
		
		StringBuffer expectedReport = new StringBuffer();
		expectedReport.append("3: blockedInvalid6_1 BLOCKED");
		
		if(!simpleErrorReport.equals(expectedReport.toString())) {
			throw new ComparisonFailedException("Expected results are not the same as actual results.");
		}
		
		os.turnOff();
	}
	
}
