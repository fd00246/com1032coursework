/**
 * OS.java
 */
package com.com1032.assignment.processscheduler;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Class used to simulate OS. Controls the process scheduler.
 * 
 * @author felipedabrantes
 */
public class OS {

	/**The global time of the OS.*/
	private Clock globalTime = new Clock();
	/**The hardware of the OS.*/
	private Hardware hardware = null;
	
	/**The class the handles the process scheduling.*/
	private ProcessScheduler ps = null;
	
	/**Directory name of folder locations to create.*/
	private final String directoryName;

	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param hardwareFileName
	 * @param directoryName
	 * 
	 * @throws IOException 
	 * @throws NumberFormatException 
	 */
	public OS(String hardwareFileName, String directoryName) throws NumberFormatException, IOException {
		this.hardware = new Hardware(hardwareFileName);
		this.directoryName = directoryName;
	}

	
	/**
	 * Runs process scheduling simulation with processes in OS.
	 * 
	 * @param algorithm wanted.
	 * @param quantum to use.
	 * 
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws IndexOutOfBoundsException if there is an error with hardware file parameters.
	 */
	public void turnOn(SchedulingAlgorithm algorithm, int quantum) throws IndexOutOfBoundsException, IOException {
		this.ps = new ProcessScheduler(this, this.directoryName);	
		
		//Starts running threads.
		this.ps.startThreads(algorithm, quantum);
	}
	
	
	/**
	 * Stops running threads.
	 * 
	 * @throws InterruptedException if timer is interrupted while sleeping.
	 */
	public void turnOff() throws InterruptedException {
		//Stops threads.
		ps.stopThreads();
		
		//Waits for threads to complete.
		TimeUnit.MILLISECONDS.sleep(2000);
		
		//Resets process scheduler and global time.
		this.ps = null;
		this.globalTime.resetTime();
	}
	
	
	/**
	 * Turns OS off and on again.
	 * 
	 * @param algorithm wanted.
	 * @param quantum to use.
	 * 
	 * @throws InterruptedException if timer is interrupted while sleeping.
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws IndexOutOfBoundsException if there is an error with hardware file parameters.
	 */
	public void resetOS(SchedulingAlgorithm algorithm, int quantum) throws InterruptedException, IndexOutOfBoundsException, IOException {
		this.turnOff();
		this.turnOn(algorithm, quantum);
	}
	
	
	/**
	 * @return the globalTime
	 */
	public Clock getGlobalTime() {
		return this.globalTime;
	}

	/**
	 * @return the hardware
	 */
	public Hardware getHardware() {
		return this.hardware;
	}

	/**
	 * @return the ps
	 */
	public ProcessScheduler getPs() {
		return this.ps;
	}
	
	/**
	 * @return the directoryName
	 */
	public String getDirectoryName() {
		return this.directoryName;
	}
	

}
