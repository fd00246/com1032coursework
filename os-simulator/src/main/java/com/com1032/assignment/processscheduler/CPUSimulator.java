/**
 * CPUSimulator.java
 */
package com.com1032.assignment.processscheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class used to simulate CPU.
 * 
 * @author felipedabrantes
 */
public class CPUSimulator extends Thread{
	/**Processes running on CPU.*/
	private List<PCB> cpuQueue = new ArrayList<PCB> ();
	
	/**A report of all the errors in the OS.*/
	private StringBuffer errorReport = new StringBuffer();
	
	/**The global time of the system.*/
	private Clock globalTime = null;
	
	/**The hardware to use.*/
	private Hardware hardware = null;

	/**Variable used to stop thread running.*/
	protected final AtomicBoolean running = new AtomicBoolean(false);
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param errorReport
	 * @param globalTime
	 * @param cpuQueue
	 * @param hardware
	 */
	public CPUSimulator(StringBuffer errorReport, Clock globalTime, List<PCB> cpuQueue, Hardware hardware) {
		this.cpuQueue = cpuQueue;	
		this.globalTime = globalTime;
		this.hardware = hardware;
		this.errorReport = errorReport;
	}
	
	
	/**
	 * Ran when thread is started.
	 */
	@Override
	public void run() {
		
		//Set running to true.
		this.running.set(true);
		
		//Runs until stopped externally.
		while(this.running.get()) {
			
			//Temporarily stop executing for however milliseconds.
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//Runs process in CPU queue.
			this.runCode();
		}
	}
	
	
	/**
	 * Main function that compiles and executes code in CPU queue.
	 */
	public void runCode() {
		if(!this.cpuQueue.isEmpty()) {
			
			if(this.cpuQueue.get(0).getState() == ProcessState.RUNNING) {
				//The process the CPU runs.
				PCB processRunning = this.cpuQueue.get(0);
				
				//Split code string into lines.
				String[] codeLines = processRunning.getCode().split("\\r\\n|\\r|\\n");
				
				//Increase program counter by one to run next line.
				processRunning.increaseProgramCounter();
				
				//Surround in try-catch.
				try {
					//Compile and run the program counter line.
					this.compileLine(processRunning, codeLines[processRunning.getProgramCounter()]);
				}
				catch (IndexOutOfBoundsException e) {
					StringBuffer errorMessage = new StringBuffer();
					errorMessage.append("\n - Error in Line ");
					errorMessage.append(processRunning.getProgramCounter());
					errorMessage.append(": ");
					errorMessage.append(processRunning);
					errorMessage.append("\n  * Code wrong format.");
					
					processRunning.setFailed(true);
					processRunning.setState(ProcessState.TERMINATED);
					
					this.errorReport.append(errorMessage);
				}
				
				//If not blocked, then decrease remaining time and global time by one.
				if(processRunning.getState() != ProcessState.BLOCKED) {
					processRunning.increaseConsecutiveTimeOnCPU();
					processRunning.decreaseRemainingTime(1);
					
					//Instruction takes 4 ticks. Each core in processor decreases it by 1.
					this.globalTime.increaseTime(5 - this.hardware.getCoresPerProcessor());
					
					//If all code finished, then terminate process.
					if(processRunning.getProgramCounter() == codeLines.length - 1) {
						processRunning.setState(ProcessState.TERMINATED);
					}
				}
			}
		}
	}
	
	
	/**
	 * Compiles the given line.
	 * 
	 * @param processRunning
	 * @param line to compile.
	 */
	public void compileLine(PCB processRunning, String line) {
		
		Map<String, Integer> variables = processRunning.getVariables();
		
		//Ignore empty line.
		if(line.equals("")) {
			return;
		}

		String command = line.split(" ")[0];
		
		//STORE NAME VALUE
		if(command.toLowerCase().equals("store")) {
			String newVariableName = line.split(" ")[1];
			Integer newVariableValue = 0;
			
			if(line.split(" ")[2].startsWith("#")) {
				newVariableValue = Integer.valueOf(line.split(" ")[2].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[2]);
				newVariableValue = storedVariableValue;
			}
			
			//Store result to new variable.
			variables.put(newVariableName, newVariableValue);
		
		}
		
		//ADD NAME VALUE1 VALUE2
		else if(command.toLowerCase().equals("add")) {
			String newVariableName = line.split(" ")[1];
			Integer newVariableValue = 0;
			
			//Find VALUE1.
			if(line.split(" ")[2].startsWith("#")) {
				newVariableValue = Integer.valueOf(line.split(" ")[2].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[2]);
				newVariableValue = storedVariableValue;
			}
			
			//Add VALUE2 to VALUE1.
			if(line.split(" ")[3].startsWith("#")) {
				newVariableValue += Integer.valueOf(line.split(" ")[3].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[3]);
				newVariableValue += storedVariableValue;
			}
			
			//Store result to new variable.
			variables.put(newVariableName, newVariableValue);
		}
		
		
		//SUB NAME VALUE1 VALUE2
		else if(command.toLowerCase().equals("sub")) {
			String newVariableName = line.split(" ")[1];
			Integer newVariableValue = 0;
			
			//Find VALUE1.
			if(line.split(" ")[2].startsWith("#")) {
				newVariableValue = Integer.valueOf(line.split(" ")[2].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[2]);
				newVariableValue = storedVariableValue;
			}
			
			//Subtract VALUE1 from VALUE2.
			if(line.split(" ")[3].startsWith("#")) {
				newVariableValue -= Integer.valueOf(line.split(" ")[3].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[3]);
				newVariableValue -= storedVariableValue;
			}
			
			//Store result to new variable.
			variables.put(newVariableName, newVariableValue);
		}
		
		
		//PRINT VALUE
		else if(command.toLowerCase().equals("print")) {
			processRunning.setState(ProcessState.BLOCKED);
		}
		
		//DISPLAY VALUE
		else if(command.toLowerCase().equals("display")) {
			processRunning.setState(ProcessState.BLOCKED);
		}
		
		//READ VALUE
		else if(command.toLowerCase().equals("read")) {
			processRunning.setState(ProcessState.BLOCKED);
		}
		
		//Throws exception if code is invalid.
		else {
			throw new IndexOutOfBoundsException();
		}
		
	}
	
}
