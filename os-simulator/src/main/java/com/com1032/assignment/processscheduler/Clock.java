/**
 * Clock.java
 */
package com.com1032.assignment.processscheduler;

/**
 * Class used to simulate system clock.
 * 
 * @author felipedabrantes
 */
public class Clock {
	/**Time of clock.*/
	private int time = 0;
	
	/**Cycle increases when time exceeds max.*/
	private int cycle = 0;
	
	
	/**
	 * Increases time by parameter given and cycle if necessary.
	 * 
	 * @param time to increase by.
	 */
	public void increaseTime(int time) {
		this.time += time;
		
		if(this.time == Integer.MAX_VALUE) {
			this.time = 0;
			this.cycle ++;
		}
	}
	
	
	/**
	 * Resets the time and cycles to zero.
	 */
	public void resetTime() {
		this.time = 0;
		this.cycle = 0;
	}
	
	
	/**
	 * @return the time
	 */
	public int getTime() {
		return this.time;
	}
	
	/**
	 * @return the cycle
	 */
	public int getCycle() {
		return this.cycle;
	}
	
	/**
	 * Returns String when object is printed.
	 */
	@Override
	public String toString() {
		return String.valueOf(this.time);
	}
}
