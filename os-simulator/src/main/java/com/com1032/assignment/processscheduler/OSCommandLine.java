/**
 * OSCommandLine.java
 */
package com.com1032.assignment.processscheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import com.com1032.assignment.tests.AllTests;

/**
 * Class to let user control OS operations from command line.
 * 
 * @author felipedabrantes
 */
public class OSCommandLine {
	
	/**
	 * Main file used when application is launched.
	 * 
	 * @param args[0] hardware file.
	 * @param args[1] location to store folders.
	 */
	public static void main(String[] args) {
		OS os = null;
		//Try to create OS object from given hardware file.
		try {
			os = new OS(args[0], args[1]);
			System.out.println("Located hardware file!");
		}
		catch(Exception e){
			System.out.println(e.getMessage());
			System.exit(0);
		}
		
		//Tries to turn on threads, error if set up goes wrong.
		try {
			os.turnOn(SchedulingAlgorithm.FIFO, 5);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
			System.exit(0);
		}
		
		
        // Create a stream for input
        BufferedReader data = null;

        data = new BufferedReader (new InputStreamReader(System.in));

        // Cycle through user or file input
        while (true) {
            try {
                // Print out the prompt for the user

                System.out.print("--> ");
                System.out.flush();

                //Read a line.
                String line = data.readLine();

                //Ctrl-D check.
                if (line == null) { 	
                	System.exit(0);  
                }
                
                //Trim off extra whitespace.
                line = line.trim();       
                
                //Ignore if command is empty.
                if (line.length() == 0) {
                	System.out.println();
                	continue;
                }

                //Tokenize command line.
                StringTokenizer cmds = new StringTokenizer (line);
                
                //Access command.
                String cmd = cmds.nextToken();

                //Reports.
                if (cmd.equalsIgnoreCase("processReport") || cmd.equalsIgnoreCase("pR")) {
                    System.out.println(os.getPs().getProcessReport());
                    continue;
                }
                
                else if (cmd.equalsIgnoreCase("errorReport") || cmd.equalsIgnoreCase("eR")) {
                    System.out.println(os.getPs().getErrorReport() + "\n");
                    continue;
                }
                
                else if (cmd.equalsIgnoreCase("IOReport") || cmd.equalsIgnoreCase("ioR")) {
                    System.out.println(os.getPs().getIOReport() + "\n");
                    continue;
                }
                
                //Algorithm setting.
                else if (cmd.equalsIgnoreCase("algorithm")) {
                	String algorithm = cmds.nextToken();
                	String quantumString = cmds.nextToken();
                	
                	int quantum = Integer.valueOf(quantumString);
                	
                	if(algorithm.equals("FIFO")) {
                		os.getPs().setAlgorithm(SchedulingAlgorithm.FIFO, quantum);
                	}
                	else if(algorithm.equals("SRTF")) {
                		os.getPs().setAlgorithm(SchedulingAlgorithm.SRTF, quantum);
                	}
                	else if(algorithm.equals("SPF")) {
                		os.getPs().setAlgorithm(SchedulingAlgorithm.SPF, quantum);
                	}
                	else if(algorithm.equals("RR")) {
                		os.getPs().setAlgorithm(SchedulingAlgorithm.RR, quantum);
                	}
                	else {
                		throw new IllegalArgumentException("No such algorithm.");
                	}
                	
                	System.out.println("Changed algorithm.");
                	
                }
                
                //Test function.
                else if (cmd.equalsIgnoreCase("test")) {
                	System.out.println("Running Tests, OS will reset after completion...");
                	os.turnOff();
                    try {
						AllTests.runAllTests(os);
						System.out.println("Completed Tests Successfully!");
						System.out.println("\nResetting OS...");
						os.turnOn(SchedulingAlgorithm.FIFO, 5);
						
					} catch (Exception e) {
						System.out.println("Tests failed.");
						System.out.println(e.getMessage());
//						e.printStackTrace();
						System.exit(0);
					}
                    
                }
                
                //Reset function.
                else if (cmd.equalsIgnoreCase("reset")) {
                	System.out.println("Reseting...");
                    os.resetOS(SchedulingAlgorithm.FIFO, 5);
                    System.out.println("Complete!");
                }
                
                //Quit function.
                else if (cmd.equalsIgnoreCase("quit")) {
                	System.out.println("Quitting...");
                    os.turnOff();
                    System.out.println("Bye!");
                    System.exit(0);
                }
                
                //Help function.
                else if (cmd.equalsIgnoreCase("help")) {
                    OSCommandLine.help();
                    continue;
                }
                
                //Ran if command is not understood.
                else {
                    System.out.println("Unknown command.");
                    continue;
                }
            }
            
            //Handler for Integer.parseInt(...).
            catch (NumberFormatException e) {
                System.out.println("Incorrect argument type.");
            }
            //Handler for nextToken().
            catch (NoSuchElementException e) {
                System.out.println("Incorrect number of elements.");
            }
            //Handler for IOType.
            catch (IllegalArgumentException e) {
            	System.out.println(e.getMessage());
            }
            //Handler for error with file operations.
            catch (IOException e) {
                System.out.println(e.getMessage());
            } 
            catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
        }

	}
    
    
    /**
     * Help will just print out a listing of the commands available on the system.
     */
    private static void help() {
    	System.out.println (" - processReport");
        System.out.println (" - errorReport");
        System.out.println (" - ioReport");
        System.out.println (" - algorithm");
        System.out.println (" - test");
        System.out.println (" - reset");
        System.out.println (" - quit");
        System.out.println (" - help");
    }

}
