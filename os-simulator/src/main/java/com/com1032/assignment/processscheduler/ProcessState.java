/**
 * ProcessState.java
 */
package com.com1032.assignment.processscheduler;

/**
 * An enumeration to access all possible states of a process.
 * 
 * @author felipedabrantes
 *
 */
public enum ProcessState {
	/**Process is being created.*/
	NEW,
	
	/**Process is being executed on a processor.*/
	RUNNING,
	
	/**Process can execute when a processor is available.*/
	READY,
	
	/**Process is waiting for event to occur.*/
	BLOCKED,
	
	/**Process has finished execution.*/
	TERMINATED
}