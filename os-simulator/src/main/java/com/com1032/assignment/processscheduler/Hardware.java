/**
 * Hardware.java
 */
package com.com1032.assignment.processscheduler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class used to store the hardware of a system.
 * 
 * @author felipedabrantes
 */
public class Hardware {
	
	/**Number of processors of the system. Max 1.*/
	private int processorNum = 0;
	/**Number of cores per processor. Max 4.*/
	private int coresPerProcessor = 0;
	/**RAM size, in GB.*/
	private int RAMSize = 0;
	/**Disk size, in GB.*/
	private int diskSize = 0;
	
	/**List of peripherals info.*/
	private List<String> peripheralsInfo = new ArrayList<String> ();
	
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param processorNum
	 * @param coresPerProcessor
	 * @param RAMSize
	 * @param diskSize
	 */
	public Hardware(int processorNum, int coresPerProcessor, int RAMSize, int diskSize) {
		this.processorNum = processorNum;
		this.coresPerProcessor = coresPerProcessor;
		this.RAMSize = RAMSize;
		this.diskSize = diskSize;
	}
	
	
	/**
	 * Constructor. Initialises fields using a text file.
	 * 
	 * @param fileName
	 * 
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws NumberFormatException if there is an error with hardware file parameters.
	 */
	public Hardware(String fileName) throws IOException, NumberFormatException {
		try {
			
			//Reads file, should be in specific format.
			File file = new File(fileName);
			Scanner fileReader = new Scanner(file);
			
			this.processorNum = Integer.valueOf(fileReader.nextLine());
			this.coresPerProcessor = Integer.valueOf(fileReader.nextLine());
			this.RAMSize = Integer.valueOf(fileReader.nextLine());
			this.diskSize = Integer.valueOf(fileReader.nextLine());
			
			while(fileReader.hasNext()) {
				this.peripheralsInfo.add(fileReader.nextLine());
			}
			
			//Closes file.
			fileReader.close();
			
			//Processor cannot be more than 4.
			if(this.processorNum > 1) {
				throw new NumberFormatException("Processor num cannot be higher than 1.");
			}
			
			//Cores per processor cannot be more than 4.
			if(this.coresPerProcessor > 4) {
				throw new NumberFormatException("Cores per processors cannot be higher than 4.");
			}
		}
		
		catch(IOException e) {
			throw new IOException("Error using Hardware file.");
		}
		catch(NumberFormatException e) {
			throw new IndexOutOfBoundsException("Error with Hardware file parameters.");
		}
	
	}

	
	/**
	 * @return the processorNum
	 */
	public int getProcessorNum() {
		return this.processorNum;
	}

	/**
	 * @return the coresPerProcessor
	 */
	public int getCoresPerProcessor() {
		return this.coresPerProcessor;
	}

	/**
	 * @return the rAMSize
	 */
	public int getRAMSize() {
		return this.RAMSize;
	}

	/**
	 * @return the diskSize
	 */
	public int getDiskSize() {
		return this.diskSize;
	}
	
	/**
	 * @return the peripheralsInfo
	 */
	public List<String> getPeripheralsInfo(){
		return this.peripheralsInfo;
	}
	
	
}
