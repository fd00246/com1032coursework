/**
 * ProcessScheduler.java
 */
package com.com1032.assignment.processscheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.com1032.assignment.IOsystem.*;

/**
 * Class used to control all of the threads that control the system's process scheduling.
 * 
 * @author felipedabrantes
 */
public class ProcessScheduler {
	
	/**New processes added to the system.*/
	private List<PCB> jobQueue = new ArrayList<PCB> ();
	/**Processes ready to be executed.*/
	private List<PCB> readyQueue = new ArrayList<PCB> ();
	/**Processes waiting for I/O.*/
	private List<PCB> blockedQueue = new ArrayList<PCB> ();
	/**Processes that have finished executing.*/
	private List<PCB> terminatedQueue = new ArrayList<PCB> ();
	/**Processes running on CPU.*/
	private List<PCB> cpuQueue = new ArrayList<PCB> ();
	
	/**The info report of the process scheduler.*/
	private StringBuffer processReport = new StringBuffer();
	
	/**A report of all the errors in the OS.*/
	private StringBuffer errorReport = new StringBuffer();
	
	/**A report of all the IO operations in the OS.*/
	private StringBuffer IOReport = new StringBuffer();
	
	
	/**The scheduling algorithm to use.*/
	private SchedulingAlgorithm algorithm = SchedulingAlgorithm.FIFO;
	
	/**The quantum to use.*/
	private int quantum = 0;
	
	/**The global time of the system.*/
	private OS os = null;
	
	/**The directory to make all folders in.*/
	private String directoryName = null;
	
	//Classes that control the separate threads.
	private ProcessCreation pc = null;
	private ProcessDispatcher pd = null;
	private CPUSimulator cpu = null;
	private IOProcessManager io = null;
	
	public ProcessScheduler(OS os, String directoryName) {
		this.os = os;
		this.directoryName = directoryName;
	}
	
	
	/**
	 * Starts running the threads needed.
	 * 
	 * @param algorithm
	 * @param quantum
	 * 
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws IndexOutOfBoundsException if there is an error with hardware file parameters.
	 */
	public void startThreads(SchedulingAlgorithm algorithm, int quantum) throws IndexOutOfBoundsException, IOException {
		//Thread responsible for creating processes from files.
		this.pc = new ProcessCreation(this.directoryName, this.errorReport, this.os.getGlobalTime(), this.jobQueue);
		
		//Thread responsible for handling processes states.
		this.pd = new ProcessDispatcher(this.os.getGlobalTime(), this.os.getHardware(), 
				this.processReport, this.algorithm, this.quantum, 
				this.jobQueue, this.readyQueue, this.blockedQueue, this.terminatedQueue, this.cpuQueue);
		
		//Update algorithm
		this.setAlgorithm(algorithm, quantum);
		
		//Thread responsible for running processes and simulating CPU.
		this.cpu = new CPUSimulator(this.errorReport, this.os.getGlobalTime(), this.cpuQueue, this.os.getHardware());
		
		//Thread responsible for running blocked processes.
		this.io = new IOProcessManager(this.directoryName, this.os.getHardware(), this.errorReport, this.IOReport, this.blockedQueue);
		
		//Start threads.
		pc.start();
		pd.start();
		cpu.start();
		io.start();
	}
	

	public void stopThreads() {
		//Sets running to false which externally stops threads.
		this.pc.running.set(false);
		this.pd.running.set(false);
		this.cpu.running.set(false);
		this.io.running.set(false);
		
		//Sets all classes controlling threads to null.
		this.pc = null;
		this.pd = null;
		this.cpu = null;
		this.io = null;
	}
	
	
	/**
	 * Sets the wanted algorithm.
	 * 
	 * @param algorithm
	 * @param quantum
	 */
	public void setAlgorithm(SchedulingAlgorithm algorithm, int quantum) {
		this.pd.setAlgorithm(algorithm, quantum);
	}
	
	/**
	 * @return the processReport
	 */
	public String getProcessReport() {
		return this.processReport.toString();
	}
	
	/**
	 * @return the errorReport
	 */
	public String getErrorReport() {
		return this.errorReport.toString();
	}
	
	/**
	 * @return the IOReport
	 */
	public String getIOReport() {
		return this.IOReport.toString();
	}
	
	/**
	 * @return the JavaIOSystem
	 */
	public JavaIOSystem getJavaIOSystem() {
		return this.io.getIOSystem();
	}

}
