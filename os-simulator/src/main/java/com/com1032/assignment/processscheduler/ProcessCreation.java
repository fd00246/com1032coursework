/**
 * ProcessCreation.java
 */
package com.com1032.assignment.processscheduler;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class used to control thread used when creating processes.
 * 
 * @author felipedabrantes
 */
public class ProcessCreation extends Thread { 
	
	/**A list of files with valid/invalid processes.*/
	public List<File> processFiles = new ArrayList<File> ();
	
	/**New processes added to the system.*/
	private List<PCB> jobQueue = new ArrayList<PCB> ();
	
	/**A report of all the errors in the OS.*/
	private StringBuffer errorReport = new StringBuffer();
	
	/**The global time of the system.*/
	private Clock globalTime = null;
	
	/**Directory name of folder locations to create.*/
	private String directoryName = null;
	
	/**Variable used to stop thread running.*/
	protected final AtomicBoolean running = new AtomicBoolean(false);
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param directoryName
	 * @param errorReport
	 * @param globalTime
	 * @param jobQueue of the system.
	 */
	public ProcessCreation(String directoryName, StringBuffer errorReport, Clock globalTime, List<PCB> jobQueue) {
		this.jobQueue = jobQueue;
		this.errorReport = errorReport;
		this.globalTime = globalTime;
		
		this.directoryName = directoryName;
		
		//Creates folders for user to input processes.
		this.createFolders();
		//Clears folders.
		this.clearFolders();
	}
	
	
	/**
	 * Ran when thread is started.
	 */
	@Override
	public void run() {
		
		//Set running to true.
		this.running.set(true);
		
		//Runs until stopped externally.
		while(this.running.get()) {
			//Fetches files where new processes are added.
			this.fetchFiles();
			
			//Creates PCBs from valid files.
			this.createProcessFromFiles();
			
			//Temporarily stop executing for however milliseconds.
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Function used to find processes in text files wanting to enter OS.
	 * 
	 * If it finds any, it will store the file.
	 */
	public void fetchFiles() {
		File folder = new File(this.directoryName + "/Processes/New_Processes");
		File[] listOfFiles = folder.listFiles();
		
		
		//Cycles through folder 'New_Processes' to look for files.
		for(File processFile: listOfFiles) {
			//If it finds file, store it.
			if(processFile.isFile()) {
				if(!this.processFiles.contains(processFile)) {
					this.processFiles.add(processFile);	
				}
			}
		}
	}
	
	
	/**
	 * Creates PCBs from the files storing the process info.
	 */
	public void createProcessFromFiles() {
		//Tries to open file.
		try {
			Scanner fileReader = new Scanner(this.processFiles.get(0));
			
			//Name of file is name of process.
			String processName = this.processFiles.get(0).getName().split("\\.")[0];
			
			//First line of file should be priority.
			int priority = Integer.valueOf(fileReader.nextLine());
			
			StringBuffer code = new StringBuffer();
			
			//Reads file line by line to read code.
			while (fileReader.hasNextLine()) {
				code.append(fileReader.nextLine());
				
				//Only adds a line break if there is another line.
				if(fileReader.hasNextLine()) {
					code.append("\n");
				}
				
			}
			
			//Makes PCB object.
			PCB pcb = new PCB(processName, priority, code.toString());
			pcb.setArrivalTime(this.globalTime.getTime());
			this.jobQueue.add(pcb);
			
			//Closes file.
			fileReader.close();
			
			//Moves file to Valid folder.
			this.fileHandling(this.processFiles.get(0), true);
			this.processFiles.remove(0);
		}
		
		//Ran when file is in wrong format.
		catch(NumberFormatException e) {
			//Moves file to Invalid folder.
			this.fileHandling(this.processFiles.get(0), false);
		}
		
		//Ran if no files in system.
		catch(IndexOutOfBoundsException e) {}
		
		//Ran if no file is found from name in system.
		catch(IOException e) {}
		
		catch(NoSuchElementException e) {}
	}
	
	
	/**
	 * Moves file to different depending if file is valid or invalid.
	 * 
	 * @param file to move.
	 * @param valid, true if file is valid.
	 */
	public void fileHandling(File file, boolean valid) {
		//Integer to make file name unique.
		int i = 0;
		
		//Runs until file is moved and a unique name is assigned.
		while(true) {
			//New file name in case file is not unique.
			String newFileName = null;
			if(valid) {
				newFileName = this.directoryName + "/Processes/Valid_Processes/" + file.getName();
			}
			else {
				newFileName = this.directoryName + "/Processes/Invalid_Processes/" + file.getName();
			}
			
			//Removes .txt extension before changing name.
			newFileName = newFileName.split("\\.")[0] + "_" + i + ".txt";
			
			i++;
			
			//Tries to move file.
			try {
				Files.move(file.toPath(), Paths.get(newFileName));
				
				if(!valid) {
					this.errorReport.append("\n -");
					this.errorReport.append(file.getName() + " wrong format. Moved to Invalid_Processes folder.");
				}
						
				//Successfully moved file so remove it from system array.
				this.processFiles.remove(0);
			}
			
			//Ran if file name already exists.
			catch(FileAlreadyExistsException e) {
				continue;
			} 
			
			//Ran if error in IO.
			catch (IOException e) {}
			
			break;
		}
	}
	
	
	/**
	 * Creates folders.
	 */
	public void createFolders() {
		//Makes parent folder as well.
		new File(this.directoryName +  "/Processes/New_Processes").mkdirs();
		new File(this.directoryName +  "/Processes/Valid_Processes").mkdirs();
		new File(this.directoryName +  "/Processes/Invalid_Processes").mkdirs();
	}
	
	
	/**
	 * Clears folders in Valid_Processes and Invalid_Processes.
	 */
	public void clearFolders() {
		File folder = new File(this.directoryName + "/Processes/Valid_Processes");
		File[] listOfFiles = folder.listFiles();
		
		//Cycles through folder 'Valid_Processes' to look for files.
		for(int i = 0; i < listOfFiles.length; i++) {
			//If it finds file, delete it.
			if(listOfFiles[i].isFile()) {
				listOfFiles[i].delete();
			}
		}
		
		folder = new File(this.directoryName + "/Processes/Invalid_Processes");
		listOfFiles = folder.listFiles();
		
		//Cycles through folder 'Valid_Processes' to look for files.
		for(int i = 0; i < listOfFiles.length; i++) {
			//If it finds file, delete it.
			if(listOfFiles[i].isFile()) {
				listOfFiles[i].delete();
			}
		}
	}

}
