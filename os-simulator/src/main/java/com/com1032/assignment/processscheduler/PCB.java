/**
 * PCB.java
 */
package com.com1032.assignment.processscheduler;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to store processes' info.
 * 
 * @author felipedabrantes
 */
public class PCB {
	/**The unique ID to identify the process.*/
	private int id = 0;
	/**The name of the process.*/
	private String name = null;
	/**The state of the process.*/
	private ProcessState state = ProcessState.NEW;
	
	/**The code of the process.*/
	private String code = null;
	/**States line number of the code.*/
	private int programCounter = -1;
	/**The variables stored in a process.*/
	private Map<String, Integer> variables = new HashMap<String, Integer> ();
	/**States whether process failed due to code error.*/
	private boolean failed = false;
	
	//---Attributes for process scheduling. ---
	/**States the importance of the process. Higher is more important.*/
	private int priority = 0;
	/**Global time at which process arrives to ready queue.*/
	private int arrivalTime = 0;
	/**Global time at which process completes its execution.*/
	private int completionTime = -1;
	/**Time required for CPU execution.*/
	private int burstTime = 0;
	/**Time remaining for process finished.*/
	private int remainingTime = 0;
	
	private int consecutiveTimeOnCPU = 0;
	
	/**
	 * Constructor. Initialises fields.
	 *
	 * @param id
	 * @param state
	 * @param priority
	 * @param arrivalTime
	 * @param completionTime
	 * @param burstTime
	 */
	public PCB(String name, int priority, String code) {
		super();
		this.name = name;
		this.priority = priority;
		this.code = code;
		
		this.burstTime = this.estimateBurstTime();
		this.remainingTime = this.burstTime;
	}
	
	
	/**
	 * Estimates burst time based on number of code lines.
	 * 
	 * @return estimated burst time.
	 */
	public int estimateBurstTime() {
		int codeLines = this.code.split("\r\n|\r|\n").length;
		return codeLines;
	}
	
	/**
	 * Increases program counter by 1.
	 */
	public void increaseProgramCounter() {
		this.programCounter++;
	}
	
	/**
	 * Decreases program counter by 1.
	 */
	public void decreaseProgramCounter() {
		this.programCounter--;
	}
	
	/**
	 * Decreases remaining time by given parameter.
	 * 
	 * @param time to decrease by.
	 */
	public void decreaseRemainingTime(int time) {
		this.remainingTime -= time;
	}
	
	/**
	 * Increase consecutive time on CPU by 1.
	 */
	public void increaseConsecutiveTimeOnCPU() {
		this.consecutiveTimeOnCPU++;
	}
	
	/**
	 * Reset consecutive time on CPU to 0.
	 */
	public void resetConsecutiveTimeOnCPU() {
		this.consecutiveTimeOnCPU = 0;
	}
	
	
	/**
	 * Sets id.
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Sets state.
	 * 
	 * @param state
	 */
	public void setState(ProcessState state) {
		this.state = state;
	}
	
	/**
	 * Sets failed.
	 * 
	 * @param failed
	 */
	public void setFailed(boolean failed) {
		this.failed = failed;
	}

	/**
	 * Sets completion time.
	 * 
	 * @param completionTime
	 */
	public void setCompletionTime(int completionTime) {
		this.completionTime = completionTime;
	}
	
	/**
	 * Sets remaining time.
	 * 
	 * @param remainingTime
	 */
	public void setRemainingTime(int remainingTime) {
		this.remainingTime = remainingTime;
	}

	/**
	 * Sets arrival time.
	 * 
	 * @param arrivalTime
	 */
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * @return the state
	 */
	public ProcessState getState() {
		return this.state;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return this.code;
	}
	
	/**
	 * @return the program counter.
	 */
	public int getProgramCounter() {
		return this.programCounter;
	}
	
	/**
	 * @return the process variables.
	 */
	public Map<String, Integer> getVariables() {
		return this.variables;
	}
	
	/**
	 * @return the failed status
	 */
	public boolean getFailed() {
		return this.failed;
	}
	
	/**
	 * @return the priority
	 */
	public int getPriority() {
		return this.priority;
	}

	/**
	 * @return the arrivalTime
	 */
	public int getArrivalTime() {
		return this.arrivalTime;
	}

	/**
	 * @return the completionTime
	 */
	public int getCompletionTime() {
		return this.completionTime;
	}
	
	/**
	 * @return the burstTime
	 */
	public int getBurstTime() {
		return this.burstTime;
	}
	
	/**
	 * @return the burstTime
	 */
	public int getRemainingTime() {
		return this.remainingTime;
	}
	
	/**
	 * @return the consecutiveTimeOnCPU
	 */
	public int getConsecutiveTimeOnCPU() {
		return this.consecutiveTimeOnCPU;
	}

	
	/**
	 * Returns String when object is printed.
	 */
	public String toString() {
		
		StringBuffer info = new StringBuffer();
		
		info.append("PCB #");
		info.append(this.id);
		info.append(" | Name: ");
		info.append(this.name);
		
		if(this.failed) {
			info.append(" | FAILED");
		}
		
		info.append(" | State: ");
		info.append(this.state);
		info.append(" | Arrival: ");
		info.append(this.arrivalTime);
		info.append(" | Burst: ");
		info.append(this.burstTime);
		info.append(" | Remaining: ");
		info.append(this.remainingTime);
		info.append(" | Completion: ");
		info.append(this.completionTime);
		info.append(" | Program Counter: ");
		info.append(this.programCounter);
		
		
		return info.toString(); 
		
	}
}
