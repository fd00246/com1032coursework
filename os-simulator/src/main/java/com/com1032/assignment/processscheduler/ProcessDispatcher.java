/**
 * ProcessDispatcher.java
 */
package com.com1032.assignment.processscheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class used to control the thread that schedules processes.
 * 
 * @author felipedabrantes
 */
public class ProcessDispatcher extends Thread {

	/**New processes added to the system.*/
	private List<PCB> jobQueue = new ArrayList<PCB> ();
	/**Processes ready to be executed.*/
	private List<PCB> readyQueue = new ArrayList<PCB> ();
	/**Processes blocked because of I/O.*/
	private List<PCB> blockedQueue = new ArrayList<PCB> ();
	/**Processes that have finished executing.*/
	private List<PCB> terminatedQueue = new ArrayList<PCB> ();
	
	private List<PCB> cpuQueue = new ArrayList<PCB> ();
	
	/**The global time of the system.*/
	private Clock globalTime = null;
	
	private Hardware hardware = null;
	
	/**The info processReport of the process scheduler.*/
	private StringBuffer processReport = null;
	
	/**The algorithm used for scheduling.*/
	private SchedulingAlgorithm algorithm = SchedulingAlgorithm.FIFO;
	
	/**The quantum used.*/
	private int quantum = 0;
	
	/**Variable used to stop thread running.*/
	protected final AtomicBoolean running = new AtomicBoolean(false);
	
	/**
	 * Constructor. Initialises fields
	 * 
	 * @param globalTime
	 * @param hardware
	 * @param processReport
	 * @param algorithm
	 * @param quantum
	 * @param jobQueue
	 * @param readyQueue
	 * @param blockedQueue
	 * @param terminatedQueue
	 * @param cpuQueue
	 * 
	 */
	public ProcessDispatcher(Clock globalTime, Hardware hardware, StringBuffer processReport, 
			SchedulingAlgorithm algorithm, int quantum,
			List<PCB> jobQueue, List<PCB> readyQueue, List<PCB> blockedQueue, List<PCB> terminatedQueue, List<PCB> cpuQueue) {
		
		super();
		this.jobQueue = jobQueue;
		this.readyQueue = readyQueue;
		this.blockedQueue = blockedQueue;
		this.terminatedQueue = terminatedQueue;
		this.cpuQueue = cpuQueue;
		
		this.globalTime = globalTime;
		this.hardware = hardware;
		
		this.algorithm = algorithm;
		this.quantum = quantum;
		
		this.processReport = processReport;
	}
	
	
	/**
	 * Ran when thread is started.
	 */
	@Override
	public void run() {
		
		//Set running to true.
		this.running.set(true);
		
		//Runs until stopped externally.
		while(this.running.get()) {
			
			//Temporarily stop executing for however milliseconds.
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			//---Job Queue Check---
			if(!this.jobQueue.isEmpty()) {
				this.readyProcess(this.jobQueue.get(0));
			}
			
			//---Termination/Block Check---
			//Checks if any CPU processes have updated.
			if(!this.cpuQueue.isEmpty()) {
				//Terminates process if process is done.
				if(this.cpuQueue.get(0).getState() == ProcessState.TERMINATED) {
					this.terminateProcess(this.cpuQueue.get(0));
				}
				
				//Block process if process is blocked.
				else if(this.cpuQueue.get(0).getState() == ProcessState.BLOCKED) {
					this.blockProcess(this.cpuQueue.get(0));
				}
			}
			
			//---Termination/Block Check---
			//Checks if any blocked processes have updated.
			if(!this.blockedQueue.isEmpty()) {
				//Terminates process if process is done.
				if(this.blockedQueue.get(0).getState() == ProcessState.TERMINATED) {
					this.terminateProcess(this.blockedQueue.get(0));
				}
				
				//Block process if process is blocked.
				else if(this.blockedQueue.get(0).getState() == ProcessState.READY) {
					this.readyProcess(this.blockedQueue.get(0));
				}
			}
			
			
			//---Empty Ready Queue---
			//If no processes are ready yet, increase time by 1.
			if(this.readyQueue.isEmpty()) {
				
				if(!this.cpuQueue.isEmpty()) {
					
					//In case of round robin, resets the time spent when above quantum.
					if(algorithm == SchedulingAlgorithm.RR && this.cpuQueue.get(0).getConsecutiveTimeOnCPU() >= quantum) {
						this.cpuQueue.get(0).resetConsecutiveTimeOnCPU();
					}
					
				}
				else {
					this.globalTime.increaseTime(1);
				}
				
				continue;
			}
			
			//---Scheduling---
			//Preemptive means processes can be interrupted. Constantly checked.
			//Non-Preemptive means processes run until finished.
			switch(this.algorithm) {
			case FIFO:
				runFIFO();
				break;
			case SRTF:
				runSRTF();
				break;
			case SPF:
				runSPF();
				break;
			case RR:
				runRR(quantum);
				break;
			}

		}
		
	}
	
	
	/**
	 * @return processReport of scheduler as string.
	 */
	public String getProcessReport() {
		return this.processReport.toString();
	}
	
	
	/**
	 * @param algorithm wanted.
	 * @param quantum wanted.
	 */
	public void setAlgorithm(SchedulingAlgorithm algorithm, int quantum) {
		this.algorithm = algorithm;
		this.quantum = quantum;
	}
	
	/**
	 * Function used to schedule a process to CPU.
	 * 
	 * @param process to schedule.
	 */
	public void scheduleProcess(PCB process) {
		//Checks if queue is empty first.
		if(!this.cpuQueue.isEmpty()) {
			//If it is not, check it is not same process being scheduled.
			if(this.cpuQueue.get(0) != process) {
				
				this.cpuQueue.get(0).resetConsecutiveTimeOnCPU();
				
				//Ran if there is already a process running.
				this.contextSwitch(this.cpuQueue.get(0));
				
				//Update scheduler attributes.
				this.processReport.append("\n- Schedule @ " + this.globalTime.getTime() + ": " + process + "\n");
				process.setState(ProcessState.RUNNING);
				this.cpuQueue.add(process);
				this.readyQueue.remove(process);
			}
		}
		else {
			//Update scheduler attributes.
			this.processReport.append("\n- Schedule @ " + this.globalTime.getTime() + ": " + process + "\n");
			process.setState(ProcessState.RUNNING);
			this.cpuQueue.add(process);
			this.readyQueue.remove(process);
		}

	}
	
	
	/**
	 * Function used when terminating a process.
	 * 
	 * @param process to terminate.
	 */
	public void terminateProcess(PCB process) {
		//Update process properties.
		process.setCompletionTime(this.globalTime.getTime());
		process.setState(ProcessState.TERMINATED);
		
		//Update scheduler info.
		this.cpuQueue.remove(process);
		this.readyQueue.remove(process);
		this.blockedQueue.remove(process);
		this.terminatedQueue.add(process);
		this.processReport.append("\n- Terminated @ " + this.globalTime.getTime() + ": " + process + "\n");
	}
	
	/**
	 * Function used when blocking a process.
	 * 
	 * @param process to block.
	 */
	public void blockProcess(PCB process) {
		//Update scheduler info.
		this.cpuQueue.remove(process);
		this.readyQueue.remove(process);
		this.blockedQueue.add(process);
		this.processReport.append("\n- Blocking @ " + this.globalTime.getTime() + ": " + process + "\n");
	}
	
	/**
	 * Function used when blocking a process.
	 * 
	 * @param process to block.
	 */
	public void readyProcess(PCB process) {
		//Update scheduler info.
		this.jobQueue.remove(process);
		this.cpuQueue.remove(process);
		this.blockedQueue.remove(process);
		this.readyQueue.add(process);
		this.processReport.append("\n- Ready @ " + this.globalTime.getTime() + ": " + process + "\n");
	}
	
	
	/**
	 * Function used when suspending a process.
	 * 
	 * @param process to suspend.
	 */
	public void contextSwitch(PCB process) {
		this.cpuQueue.remove(process);
		this.readyQueue.add(process);
		this.processReport.append("\n- Suspending @ " + this.globalTime.getTime() + ": " + process + "\n");
		process.setState(ProcessState.READY);
	}
	
	
	/**
	 * Runs the FIFO algorithm.
	 * 
	 * First-In-First-Out.
	 */
	public void runFIFO() {
		//Non-Preemptive, won't access new process from ready queue if one is already running.
		if(this.cpuQueue.isEmpty()) {
			PCB currentProcess = this.readyQueue.get(0);
			
			this.scheduleProcess(currentProcess);
		}
		else {
			this.scheduleProcess(this.cpuQueue.get(0));
		}
		
	}
	
	
	/**
	 * Runs the SPF algorithm.
	 * 
	 * Shortest Process First.
	 */
	public void runSPF() {
		//Non-Preemptive, won't access new process from ready queue if one is already running.
		if(this.cpuQueue.isEmpty()) {
			
			//Find smallest burst time process.
			PCB currentProcess = this.readyQueue.get(0);
			for(PCB process: this.readyQueue) {
				if(process.getBurstTime() < currentProcess.getBurstTime()) {
					currentProcess = process;
				}
			}
			
			this.scheduleProcess(currentProcess);
		}
		else {
			this.scheduleProcess(this.cpuQueue.get(0));
		}
		
	}
	
	
	/**
	 * Runs the SRTF algorithm.
	 * 
	 * Shortest Remaining Time First.
	 */
	public void runSRTF() {
		//Find smallest remaining time process.
		PCB currentProcess = this.readyQueue.get(0);
		for(PCB process: this.readyQueue) {
			if(process.getRemainingTime() < currentProcess.getRemainingTime()) {
				currentProcess = process;
			}
		}
		
		if(!this.cpuQueue.isEmpty()) {
			if(currentProcess.getRemainingTime() < this.cpuQueue.get(0).getRemainingTime()) {
				this.scheduleProcess(currentProcess);
			}
		}
		else {
			this.scheduleProcess(currentProcess);
		}

		
	}
	
	
	/**
	 * Runs the RR algorithm with the given quantum..
	 * 
	 * Round Robin.
	 * 
	 * @param quantum to use.
	 */
	public void runRR(int quantum) {
		PCB currentProcess = this.readyQueue.get(0);
		
		if(!this.cpuQueue.isEmpty()) {
			//One core = 4 ticks per instruction, therefore quantum + 3
			//Two core = 3 ticks per instruction, therefore quantum + 2
			//Three core = 2 ticks per instruction, therefore quantum + 1
			//Four core = 1 ticks per instruction, therefore quantum + 0
			
			if(this.cpuQueue.get(0).getConsecutiveTimeOnCPU() >= (quantum + (4 - this.hardware.getCoresPerProcessor()))) {
				this.scheduleProcess(currentProcess);
			}
		}
		else {
			this.scheduleProcess(currentProcess);
		}
	}
	
}
