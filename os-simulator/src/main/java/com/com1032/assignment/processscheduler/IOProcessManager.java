/**
 * IOProcessManager.java
 */
package com.com1032.assignment.processscheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import com.com1032.assignment.IOsystem.JavaIOSystem;

/**
 * Class used to manage and run blocked processes interacting with IO devices.
 * 
 * @author felipedabrantes
 */
public class IOProcessManager extends Thread {

	/**A report of all the errors in the OS.*/
	private StringBuffer errorReport = new StringBuffer();
	
	/**A report of all the IO operations in the OS.*/
	private StringBuffer IOReport = new StringBuffer();
	
	/**Processes waiting for I/O.*/
	private List<PCB> blockedQueue = new ArrayList<PCB> ();
	
	/**The IO System used to control peripherals.*/
	private JavaIOSystem IOSystem = null;
	
	/**Variable used to stop thread running.*/
	protected final AtomicBoolean running = new AtomicBoolean(false);
	
	/**
	 * Constructor. Initialises fields.
	 * 
	 * @param directoryName
	 * @param hardware
	 * @param errorReport
	 * @param IOReport
	 * @param blockedQueue
	 * 
	 * @throws IOException if there is an error accessing hardware file.
	 * @throws IndexOutOfBoundsException if there is an error with hardware file parameters.
	 * 
	 */
	public IOProcessManager(String directoryName, Hardware hardware, 
			StringBuffer errorReport, StringBuffer IOReport, List<PCB> blockedQueue) throws IndexOutOfBoundsException, IOException {
		
		this.errorReport = errorReport;
		this.blockedQueue = blockedQueue;
		this.IOReport = IOReport;
		
		this.IOSystem = new JavaIOSystem(directoryName, hardware);
	}
	
	
	/**
	 * Ran when thread is started.
	 */
	@Override
	public void run() {
		
		//Set running to true.
		this.running.set(true);
		
		//Runs until stopped externally.
		while(this.running.get()) {
			
			//Temporarily stop executing for however milliseconds.
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.runBlockedProcess();
		}
	}
	
	
	/**
	 * Function used when running a blocked process.
	 */
	public void runBlockedProcess() {
		
		if(!this.blockedQueue.isEmpty()) {
			
			if(this.blockedQueue.get(0).getState() == ProcessState.BLOCKED) {
				//The process the CPU runs.
				PCB processRunning = this.blockedQueue.get(0);
				
				//Split code string into lines.
				String[] codeLines = processRunning.getCode().split("\\r\\n|\\r|\\n");
				
				//Surround in try-catch.
				try {
					//Compile and run the program counter line.
					this.compileLine(processRunning, codeLines[processRunning.getProgramCounter()]);
				}
				
				//Ran when there is a syntax error in code.
				catch (IndexOutOfBoundsException e) {
					StringBuffer errorMessage = new StringBuffer();
					errorMessage.append("\n - Error in Line ");
					errorMessage.append(processRunning.getProgramCounter());
					errorMessage.append(": ");
					errorMessage.append(processRunning);
					errorMessage.append("\n  * Code wrong format.");
					
					processRunning.setFailed(true);
					processRunning.setState(ProcessState.TERMINATED);
					
					this.errorReport.append(errorMessage);
				}
				
				//Ran when input buffer is invalid.
				catch (NumberFormatException e) {
					StringBuffer errorMessage = new StringBuffer();
					errorMessage.append("\n - Error in Line ");
					errorMessage.append(processRunning.getProgramCounter());
					errorMessage.append(": ");
					errorMessage.append(processRunning);
					errorMessage.append("\n  * Input device must only contain integers.");
					
					processRunning.setFailed(true);
					processRunning.setState(ProcessState.TERMINATED);
					
					this.errorReport.append(errorMessage);
				}
				
				catch (IllegalArgumentException | IOException e) {
					StringBuffer errorMessage = new StringBuffer();
					errorMessage.append("\n - Error in Line ");
					errorMessage.append(processRunning.getProgramCounter());
					errorMessage.append(": ");
					errorMessage.append(processRunning);
					errorMessage.append("\n  * ");
					errorMessage.append(e);
					
					processRunning.setFailed(true);
					processRunning.setState(ProcessState.TERMINATED);
					
					this.errorReport.append(errorMessage);
				}
				
				//Decrease expected remaining time.
				processRunning.decreaseRemainingTime(1);
				
				//Reset time used on CPU.
				processRunning.resetConsecutiveTimeOnCPU();
				
				//If process got an error, then leave it.
				if(!processRunning.getFailed()) {
					//If all code finished, then terminate process.
					if(processRunning.getProgramCounter() == codeLines.length - 1) {
						processRunning.setState(ProcessState.TERMINATED);
					}
					else {
						processRunning.setState(ProcessState.READY);
					}
				}
			}
		}
	}
	
	
	/**
	 * Function used to compile a code line.
	 * 
	 * @param processRunning
	 * @param line to compile.
	 * 
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void compileLine(PCB processRunning, String line) throws IllegalArgumentException, IOException {
		
		Map<String, Integer> variables = processRunning.getVariables();
		
		String command = line.split(" ")[0];
		
		//PRINT VALUE DEVICE
		if(command.toLowerCase().equals("print")) {
			Integer variableValue = null;
			
			if(line.split(" ")[1].startsWith("#")) {
				variableValue = Integer.valueOf(line.split(" ")[1].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[1]);
				variableValue = storedVariableValue;
			}
			
			String deviceName = line.split(" ")[2];
			
			this.print(variableValue, deviceName);
		}
		
		
		//DISPLAY VALUE DEVICE
		if(command.toLowerCase().equals("display")) {
			Integer variableValue = null;
			
			if(line.split(" ")[1].startsWith("#")) {
				variableValue = Integer.valueOf(line.split(" ")[1].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[1]);
				variableValue = storedVariableValue;
			}
			
			String deviceName = line.split(" ")[2];
			
			this.display(variableValue, deviceName);
		}
		
		
		//READ STORE SIZE DEVICE
		if(command.toLowerCase().equals("read")) {
			String storeVariable = line.split(" ")[1];
			Integer sizeVariable = null;
			
			if(line.split(" ")[2].startsWith("#")) {
				sizeVariable = Integer.valueOf(line.split(" ")[2].substring(1));
			}
			else {
				Integer storedVariableValue = variables.get(line.split(" ")[2]);
				sizeVariable = storedVariableValue;
			}
			
			String deviceName = line.split(" ")[3];
			
			this.read(storeVariable, sizeVariable, deviceName);
		}
	}
	
	
	/**
	 * Function used to run print command.
	 * 
	 * @param value
	 * @param deviceName
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public void print(int value, String deviceName) throws IllegalArgumentException, IOException {
		this.IOSystem.write(deviceName, String.valueOf(value));
		
		this.IOReport.append("\n - Printed to ");
		this.IOReport.append(deviceName);
	}
	
	
	/**
	 * Function used to run display command.
	 * @param value
	 * @param deviceName
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 */
	public void display(int value, String deviceName) throws IllegalArgumentException, IOException {
		this.IOSystem.write(deviceName, String.valueOf(value));
		
		this.IOReport.append("\n - Displayed to ");
		this.IOReport.append(deviceName);
	}
	
	
	/**
	 * Function used to run read command.
	 * 
	 * @param storeVariable
	 * @param size
	 * @param deviceName
	 * 
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws NumberFormatException
	 */
	public void read(String storeVariable, int size, String deviceName) throws IllegalArgumentException, IOException, NumberFormatException {
		String result = this.IOSystem.read(deviceName, size);
		
		this.blockedQueue.get(0).getVariables().put(storeVariable, Integer.valueOf(result));
		
		this.IOReport.append("\n - Read from ");
		this.IOReport.append(deviceName);
	}
	
	
	/**
	 * @return errorReport
	 */
	public String getErrorReport() {
		return this.errorReport.toString();
	}
	
	/**
	 * @return IOReport
	 */
	public String getIOReport() {
		return this.IOReport.toString();
	}
	
	
	/**
	 * @return IOSystem
	 */
	public JavaIOSystem getIOSystem() {
		return this.IOSystem;
	}

}
