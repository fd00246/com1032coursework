/**
 * SchedulingAlgorithm.java
 */
package com.com1032.assignment.processscheduler;

/**
 * Enumeration of the scheduling algorithms.
 * 
 * Preemptive: Might temporarily interrupt the process scheduled to resume it later.
 * 
 * Non-Preemptive: Process scheduled does not stop until it is completed.
 * 
 * @author felipedabrantes
 *
 */
public enum SchedulingAlgorithm {
	
	/**First-In-First-Out.*/
	FIFO(false),
	/**Shortest Process First.*/
	SPF(false),
	/**Shortest Remaining Time First.*/
	SRTF(true),
	/**Round Robin.*/
	RR(true);
	

	/**True if algorithm is preemptive.*/
	private boolean preemptive;
	
	
	/**
	 * Constructor.
	 * 
	 * @param preemptive or not.
	 */
	private SchedulingAlgorithm(boolean preemptive) {
		this.preemptive = preemptive;
	}
	
	
	/**
	 * @return preemptive or not.
	 */
	public boolean getPreemptive() {
		return this.preemptive;
	}
}
