# COM1032Coursework

### An Operating System Simulator developed in Java.

**Subsystems of the simulator include:**
- A process scheduler.
- An input/output subsystem.
- A simluated CPU that compiles and execute user written programs.

<br>

#### **Full User and Technical Report [here](../Report_FD00246.docx)**.
